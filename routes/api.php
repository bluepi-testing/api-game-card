<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/get-card', [\App\Http\Controllers\Api\IndexController::class, 'index']);
Route::post('/push-log', [\App\Http\Controllers\Api\IndexController::class, 'logs']);
Route::post('/submit-score', [\App\Http\Controllers\Api\IndexController::class, 'submitScore']);
