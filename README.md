# API Game Card Documentation
โปรเจ็คจะมี 2 ส่วนแยกระหว่าง API และ Frontend ซึ่งทั้ง 2 โปรเจ็ค เขียนด้วย Laravel ในส่วนนี้คือส่วนของ ( API )

ซึ่ง Documentation นี้จะบอกถึงการติดตั้ง Project บน server ของ Linux แบบ แยก server คนละเครื่อง 

## ติดตั้ง ​Docker (หากยังไม่มี)
1 ติดตั้ง Docker repository

```bash
$ sudo apt-get update

$ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
```

2 เพิ่ม Docker’s official GPG key:
```bash
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

3 ติดตั้ง DOCKER ENGINE

```bash
$ sudo apt-get update
$ sudo apt-get install docker-ce docker-ce-cli containerd.io
```

## ติดตั้ง Docker-compose (หากยังไม่มี)
1 Download compose version ล่าสุด

```bash
$ sudo curl -L "https://github.com/docker/compose/releases/download/1.28.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```

2 ปรับ Permission
```bash
$ sudo chmod +x /usr/local/bin/docker-compose
```

## ติดตั้ง โปรเจ็ค Laravel 

1 clone git

```bash
$ git clone https://gitlab.com/bluepi-testing/api-game-card.git
```

2 run docker-compose 
```bash
$ docker-compose up -d
```

3 สร้างไฟล์ .env ผ่าน .env.example
```bash
$ cp .env.example .env
```

4 ติดตั้ง composer และสั่ง migrate table 
```bash
$ docker exec -it web bash
$ cd /app
$ composer install
$ php artisan migrate
$ exit
```
## API Routing
```bash
[POST]/api/get-card //สำหรับ random Array ตัวเลข ทั้ง 12 ตัวของ ไพ่แต่ละใบ และ จะ return ค่าของ myBestScore และ globalBestScore ด้วย 

{
  'message' => null,
  'game_id' => null,
  'fdId' => null,
  'game_seesion_hash' => null,
  'globalBestScore' => null,
  'myBestScore' => null
}

---------------------------

[POST]/api/push-log //สำหรับ save ทุกการ คลิก ของ user ว่าคลิกที่ ไพ่ใบไหน ได้ value ไหนกลับไป การคลิกนี้เป็นของเกมไหน และ จำนวนคลิก ที่เท่าไหร่

{
  'isValid' => null,
  'totalClick' => null,
  'value' => null
}

---------------------------

[POST]/api/submit-score // ทำงานต่อเมื่อเปิดไพ่ครบทุกใบ จะทำการ เก็บคะแนน ลงไปในเกมนั้น ๆ  
{
  []
}

---------------------------
```
